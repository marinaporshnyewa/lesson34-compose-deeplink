package com.example.mycompose.repository

import com.example.mycompose.network.NetworkManager

class HeroesRepository(private val manager: NetworkManager) {

    suspend fun getHeroes(page: String?) =
        manager.provideUnauthorizedCachedRequestsApi().getList(page)

}