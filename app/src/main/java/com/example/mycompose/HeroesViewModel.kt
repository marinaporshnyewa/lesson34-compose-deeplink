package com.example.mycompose

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.example.mycompose.model.Data
import com.example.mycompose.paging.DisneyDataSource


class HeroesViewModel(
) : ViewModel() {

    val result = Pager(PagingConfig(20), 1) {
        DisneyDataSource()
    }.flow.cachedIn(viewModelScope)


}