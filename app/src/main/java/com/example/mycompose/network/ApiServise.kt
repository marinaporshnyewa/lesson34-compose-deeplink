package com.example.mycompose.network

import com.example.mycompose.model.DisneyHeroes
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("/characters")
    suspend fun getList(
        @Query("page") page: String? = null
    ): Response<DisneyHeroes>

}