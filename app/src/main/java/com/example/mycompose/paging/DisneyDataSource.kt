package com.example.mycompose.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.mycompose.model.Data
import com.example.mycompose.network.NetworkManager
import com.example.mycompose.repository.HeroesRepository
import java.lang.Exception

class DisneyDataSource(

) : PagingSource<Int, Data>() {

    private var totalPages = 0

    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, Data> {


        return try {
            val nextPageNumber = params.key ?: 1
            val response = HeroesRepository(NetworkManager).getHeroes(nextPageNumber.toString())
            totalPages = response.body()?.totalPages ?: 0
            if (totalPages < nextPageNumber + 1) {
                LoadResult.Page(
                    data = response.body()?.data ?: arrayListOf(),
                    prevKey = null,
                    nextKey = null
                )

            } else {
                LoadResult.Page(
                    data = response.body()?.data ?: arrayListOf(),
                    prevKey = null,
                    nextKey = nextPageNumber + 1
                )
            }

        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Data>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }
}